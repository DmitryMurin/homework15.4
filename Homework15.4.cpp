#include <iostream>

void PrintNumbers(int limit, bool lsOdd)
{
    for (int i = 0 + lsOdd; i <= limit; i += 2)
    {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}

int main()
{
    const int x = 30;

    std::cout << "Even numbers from number " << x << " - ";
    PrintNumbers(x, false);

    std::cout << "Uneven numbers from number " << x << " - ";
    PrintNumbers(x, true);

    return 0;
}
